package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/pkg/errors"
	git "gopkg.in/src-d/go-git.v4"
)

type Project struct {
	Archived          bool   `json:"archived"`
	EmptyRepo         bool   `json:"empty_repo"`
	HTTPURLToRepo     string `json:"http_url_to_repo"`
	Name              string `json:"name"`
	PathWithNamespace string `json:"path_with_namespace"`
	WikiEnabled       bool   `json:"wiki_enabled"`
}

type Projects []Project

func main() {
	log.Print("Started")
	defer func() {
		log.Print("Ended")
	}()

	var gitlabGroup = flag.String("id", "", "GitLab group ID")
	var gitlabToken = os.Getenv("GITLAB_TOKEN")
	var gitlabUser = flag.String("u", "", "GitLab username")
	var targetPath = flag.String("d", "", "Destination")

	flag.Parse()

	info, err := os.Stat(*targetPath)
	if err != nil {
		log.Fatalf("Could not stat target path `%s`: %s", *targetPath, err.Error())
	} else if !info.IsDir() {
		log.Fatalf("Path `%s` is not directory", *targetPath)
	}

	// Since we want to fetch only one "group" so we don't need to repeat directories in target directory. Target directory itself is the "group" so to say.
	// User is though as group here
	if gitlabGroup != nil && len(*gitlabGroup) > 0 && gitlabUser != nil && len(*gitlabUser) > 0 {
		log.Fatalf("Only group ID or username argument allowed\n")
	}

	var projects Projects

	if gitlabGroup != nil && len(*gitlabGroup) > 0 {
		p, err := fetchGroupProjects(*gitlabGroup, gitlabToken)
		if err != nil {
			log.Fatalf("Could not fetch group projects: %s", err.Error())
		}
		projects = append(projects, p...)
	}

	if gitlabUser != nil && len(*gitlabUser) > 0 {
		p, err := fetchUserProjects(*gitlabUser, gitlabToken)
		if err != nil {
			log.Fatalf("Could not fetch user projects: %s", err.Error())
		}
		projects = append(projects, p...)
	}

	re := regexp.MustCompile("^[^/]+/")
	for _, project := range projects {
		// Remove first part of namespace(main group)
		path := string(re.ReplaceAll([]byte(project.PathWithNamespace), []byte("")))

		// Fetch repository
		if !project.EmptyRepo {
			directory := fmt.Sprintf("%s/%s", *targetPath, path)
			err := do(project.HTTPURLToRepo, directory, gitlabToken)
			if err != nil {
				log.Fatalf("Unable to do repository `%s`: %s\n", directory, err.Error())
			}
		}

		// Fetch Wiki
		if project.WikiEnabled {
			re := regexp.MustCompile("\\.git$")
			directory := fmt.Sprintf("%s/%s.wiki", *targetPath, path)
			url := re.ReplaceAllString(project.HTTPURLToRepo, ".wiki.git")
			err := do(url, directory, gitlabToken)
			if err != nil {
				log.Fatalf("Unable to do wiki `%s`: %s\n", directory, err.Error())
			}
		}
	}
}

func do(repoURL string, directory string, gitlabToken string) error {
	var result error
	defer func() {
		var r string
		if result != nil {
			r = fmt.Sprintf("Error => %s", result.Error())
		} else {
			r = "done"
		}
		log.Printf("%s ==> %s ... %s", repoURL, directory, r)
	}()

	_, err := os.Stat(directory)
	if os.IsNotExist(err) {
		err := os.MkdirAll(directory, 0700)
		if err != nil {
			result = errors.Wrap(err, fmt.Sprintf("unable to create directory `%s`", directory))
			return nil
		}
	}

	d, err := os.Open(directory)
	if err != nil {
		result = errors.Wrap(err, fmt.Sprintf("unable to open directory `%s`", directory))
		return nil
	}

	names, err := d.Readdirnames(1)
	if err != io.EOF && err != nil {
		result = errors.Wrap(err, fmt.Sprintf("unable to read directory `%s`", directory))
		return nil
	}

	if len(names) <= 0 {
		url := strings.Replace(repoURL, "//", fmt.Sprintf("//oauth:%s@", gitlabToken), 1)

		_, err = git.PlainClone(directory, false, &git.CloneOptions{
			URL: url,
		})
		if err != nil && err.Error() == "repository is empty" {
			return nil
		} else if err != nil && err.Error() == "repository does not exists" {
			return nil
		} else if err != nil {
			result = errors.Wrap(err, fmt.Sprintf("unable to fetch repository `%s`", url))
			return nil
		}
	} else {
		repository, err := git.PlainOpen(directory)
		if err != nil {
			result = errors.Wrap(err, fmt.Sprintf("unable to open repository `%s`", directory))
			return nil
		}

		worktree, err := repository.Worktree()
		if err != nil {
			result = errors.Wrap(err, fmt.Sprintf("unable to open work tree `%s`", directory))
			return nil
		}

		err = worktree.Pull(&git.PullOptions{
			RemoteName: "origin",
		})
		if err != nil && err.Error() == "already up-to-date" {
			return nil
		} else if err != nil {
			result = errors.Wrap(err, fmt.Sprintf("unable to pull repository `%s`", directory))
			return nil
		}
	}

	return nil
}

func fetchGroupProjects(gitlabGroup string, gitlabToken string) (Projects, error) {
	log.Printf("Get GitLab groups `%s` projects\n", gitlabGroup)

	url := fmt.Sprintf("https://gitlab.com/api/v4/groups/%s/projects?include_subgroups=1", gitlabGroup)

	return fetchProjects(url, gitlabToken)
}

func fetchUserProjects(gitlabUser string, gitlabToken string) (Projects, error) {
	log.Printf("Get GitLab users `%s` projects\n", gitlabUser)

	url := fmt.Sprintf("https://gitlab.com/api/v4/users/%s/projects?include_subgroups=1", gitlabUser)

	return fetchProjects(url, gitlabToken)
}

func fetchProjects(url, gitlabToken string) (Projects, error) {
	var perPage int = 100
	var page int = 0
	var p Projects
	var projects Projects
	client := http.Client{}

	for ok := true; ok; ok = len(p) >= perPage {
		page++
		url := fmt.Sprintf("%s&per_page=%d&page=%d", url, perPage, page)
		request, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return projects, errors.Wrap(err, "unable to create new request for projects")
		}

		request.Header.Set("PRIVATE-TOKEN", gitlabToken)

		response, err := client.Do(request)
		if err != nil {
			return projects, errors.Wrap(err, "unable to request projects")
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return projects, errors.Wrap(err, "unable to read projects response body")
		}

		if response.StatusCode != 200 {
			return projects, errors.New(fmt.Sprintf("unable to fetch projects `%s`: %s", url, response.Status))
		}

		p = nil
		err = json.Unmarshal(body, &p)
		if err != nil {
			return projects, errors.Wrap(err, "unable to parse projects response body")
		}

		projects = append(projects, p...)
	}

	return projects, nil
}
