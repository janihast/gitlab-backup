# Description

Clone GitLab group projects to target path

## Build

```sh
go build -o gitlab-backup
strip -s gitlab-backup
```

## Run

```sh
GITLAB_TOKEN=<token> gitlab-backup -id <group id> -d <path to directory>
```

## Cron

/etc/cron.daily/gitlab-backup
```sh
#!/usr/bin/env bash

GITLAB_TOKEN=<token> /usr/bin/gitlab-backup -id <group id> -d <path to directory > /var/log/gitlab-backup/<group name>.log 2>&1
```

## systemd timer

1. Copy binary to `/usr/local/sbin`

2. Copy .service and .timer from `systemd` to `/etc/systemd/system`

3. Create configuration file to `/etc/default/gitlab-backup-<name>`
```sh
DEST="/path/to"
GITLAB_TOKEN="token"
GROUP_ID="group id"
```

4. Enable timer
```sh
$ sudo systemctl daemon-reload
$ sudo systemctl enable gitlab-backup@<name>.timer
```
