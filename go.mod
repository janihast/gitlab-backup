module gitlab.com/project-kumi/applications/gitlab-backup.git

go 1.16

require (
	github.com/pkg/errors v0.8.1
	gopkg.in/src-d/go-git.v4 v4.13.1
)
